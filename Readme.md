# Rocky Linux with steamcmd

This container image is based on Rocky Linux and contains steamcmd.

The structure is loosely based on [cm2network/steamcmd](https://hub.docker.com/r/cm2network/steamcmd).

### Examples

Check out [this repository](https://gitlab.com/DerEnderKeks/docker-satisfactory) to see how this image may be used.