FROM rockylinux:8.5
LABEL maintainer="DerEnderKeks"

ENV USER steam
ENV PUID 1000
ENV HOMEDIR /home/$USER
ENV WORKDIR $HOMEDIR/steamcmd
ENV PATH "$WORKDIR:${PATH}"

RUN set -x && \
    dnf upgrade -y && \
    dnf install curl glibc-langpack-en glibc.i686 libstdc++.i686 -y && \
    dnf clean all && \
    useradd -U -u $PUID -m $USER && \
    su $USER -c \
        "set -x && \
        mkdir -p $WORKDIR && \
        curl -sL https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz | tar xzf - -C $WORKDIR && \
        $WORKDIR/steamcmd.sh +quit && \
        mkdir -p $HOMEDIR/.steam/sdk32 && \
        ln -s $WORKDIR/linux32/steamclient.so $HOMEDIR/.steam/sdk32/steamclient.so" && \
    rm -rf /tmp/*

USER $USER
WORKDIR $WORKDIR